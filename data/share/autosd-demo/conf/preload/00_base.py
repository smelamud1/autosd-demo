import autosd_demo.utils
from autosd_demo.utils.addons import get_addons_metadata

BASE_DIR = autosd_demo.utils.find_project_settings().parent.as_posix()
BUILD_DIR = (autosd_demo.utils.find_project_settings().parent / "build").as_posix()
CENTOS_SIG = {
    "autosd_demo": {
        "share_dir": autosd_demo.AUTOSD_DEMO_DATA_PATH.as_posix(),
        "compose_dir": autosd_demo.AUTOSD_DEMO_COMPOSE_PATH.as_posix(),
        "playbooks_dir": (autosd_demo.AUTOSD_DEMO_DATA_PATH / "ansible").as_posix(),
        "template_image_path": (
            autosd_demo.AUTOSD_DEMO_DATA_PATH / "image.mpp.yaml.j2"
        ).as_posix(),
        "template_systemd_container_path": (
            autosd_demo.AUTOSD_DEMO_DATA_PATH / "systemd.container.j2"
        ).as_posix(),
        "addons": get_addons_metadata(),
    }
}
