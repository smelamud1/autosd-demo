#!/usr/bin/python3


class FilterModule:
    def filters(self):
        return {"pascal_case": self.pascal_case}

    def pascal_case(self, input_text):
        components = input_text.lower().split("_")
        return "".join(component.title() for component in components)
