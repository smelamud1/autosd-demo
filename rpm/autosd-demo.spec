%define _version 0.0.1
%define _release 1

%global debug_package %{nil}

Name:           autosd-demo
Version:        %{_version}
Release:        %{_release}%{?dist}
Summary: A set of tools to run autosd demos
License: MIT

URL: https://gitlab.com/roberto-majadas/autosd-demo
Source: %{name}-%{version}.tar.gz

Requires: git
BuildRequires: git

%description
This package contains a set of tools to run autosd demos

BuildArch: noarch

%prep
%autosetup -p1

%generate_buildrequires
%pyproject_buildrequires -x tests

%build
%pyproject_wheel

%install
%pyproject_install

%check
export RPM_CHECK=1
%pytest

%files
%{_bindir}/autosd-demo
%{_datadir}/autosd-demo
%{python3_sitelib}/autosd_demo/
%{python3_sitelib}/autosd_demo-*.dist-info/
%license LICENSE

%changelog
* Fri Apr 06 2024 Roberto Majadas <rmajadas@redhat.com>
-
