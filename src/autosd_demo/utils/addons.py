from autosd_demo import AUTOSD_DEMO_DATA_PATH


def get_addons_metadata():
    addons_metadata = {}
    for addon_path in (AUTOSD_DEMO_DATA_PATH / "addons").iterdir():
        if (addon_path / "config.toml").is_file():
            addons_metadata[addon_path.name] = {"base_dir": str(addon_path.absolute())}

    return addons_metadata
