import sys
from pathlib import Path
from typing import Optional


def in_virtualenv():
    return sys.prefix != sys.base_prefix


def in_notebook():
    try:
        import IPython

        if "IPKernelApp" not in IPython.get_ipython().config:
            return False
    except ImportError:  # pragma: no cover
        return False
    except AttributeError:  # pragma: no cover
        return False
    return True


def find_project_settings(path: Optional[Path] = None) -> Optional[Path]:
    if path is None:
        current_path = Path.cwd()
    else:
        current_path = path
    project_settings_path = current_path.joinpath(".autosd-demo.toml")

    if project_settings_path.exists() and project_settings_path.is_file():
        return project_settings_path

    if current_path.parent == current_path:
        return None

    return find_project_settings(current_path.parent)


def in_autosd_demo():
    return find_project_settings() is not None
