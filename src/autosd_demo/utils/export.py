import os
from contextlib import chdir
from pathlib import Path


def create_exported_tarball(build_path):
    src_path = build_path.joinpath("src")
    osbuild_path = build_path.joinpath("osbuild-manifests")
    src_path.rename(osbuild_path)
    tarball_path = Path.cwd() / "osbuild-manifests.tar.gz"
    with chdir(build_path):
        os.system(f"tar czf {tarball_path.as_posix()} osbuild-manifests")
