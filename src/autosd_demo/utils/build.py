import hashlib
from collections import UserDict
from functools import cached_property
from pathlib import Path

from jinja2 import Template
from passlib.handlers.sha2_crypt import sha256_crypt

from autosd_demo.settings import settings


class BuildEnv(UserDict):
    def __init__(self, host, osbuild=False):
        super().__init__()
        self.host = host
        self._set_build_env_config()
        if osbuild:
            self._set_osbuild_config()

    @property
    def is_remote(self):
        return True if self.host else False

    @cached_property
    def conf(self):
        return {key.lower(): val for key, val in settings.as_dict().items()}

    @cached_property
    def base_dir(self):
        if self.is_remote:
            return (
                self.conf["remote_host"][self.host]["base_dir"]
                if self.conf_exists("remote_host", self.host, "base_dir")
                else "~"
            )

        return self.conf["base_dir"]

    @property
    def build_id(self):
        return self["build"]["build_id"]

    @cached_property
    def build_dir(self):
        if self.is_remote:
            return (
                self.conf["remote_host"][self.host]["build_dir"]
                if self.conf_exists("remote_host", self.host, "build_dir")
                else str(Path(self.base_dir) / "build")
            )

        return self.conf["build_dir"]

    def conf_exists(self, *keys):
        return self.key_exists(self.conf, *keys)

    @staticmethod
    def key_exists(element, *keys):
        _element = element
        for key in keys:
            try:
                _element = _element[key]
            except KeyError:
                return False
        return True

    def __repr__(self):
        return f"{type(self).__name__}({self.data})"

    def _set_build_env_config(self):
        build_env = {"conf": self.conf, "build": {}}
        build_env["build"]["is_remote"] = self.is_remote
        build_env["build"]["base_dir"] = self.base_dir
        build_env["build"]["build_dir"] = self.build_dir
        build_env["build"]["src"] = self._get_build_src_contents()
        self.update(build_env)

    def _get_build_src_contents(self):
        src_contents = {
            "directories": [],
            "files": [],
            "templated_files": [],
            "fetch_files": [],
        }

        self._get_build_src_quadlets(src_contents)
        self._get_build_src_extra_files(src_contents)
        self._get_build_src_custom_containers_files(src_contents)

        self._reduce_build_src_directory_list(src_contents)

        return src_contents

    def _reduce_build_src_directory_list(self, src_contents):
        src_contents["directories"] = sorted(list(set(src_contents["directories"])))
        dir_paths = [Path(d) for d in src_contents["directories"]]
        reduced_directory_list = []
        for index, dir_x in enumerate(dir_paths):
            include_path = True
            for dir_y in dir_paths[index + 1 :]:
                if dir_x in list(dir_y.parents):
                    include_path = False
                    break
            if include_path:
                reduced_directory_list.append(dir_x.as_posix())

        src_contents["directories"] = reduced_directory_list

    def _get_build_src_custom_containers_files(self, src_contents):
        if self.conf_exists("containers"):
            for container_id, container_data in self.conf["containers"].items():
                if "container_context" in container_data:
                    context_path = Path(container_data["container_context"])
                    if not context_path.is_absolute():
                        context_path = Path(self.conf["base_dir"]) / context_path

                    list_files = [f for f in context_path.glob("**/*") if f.is_file()]

                    for file in list_files:
                        path_template = Template(
                            file.as_posix(),
                            variable_start_string="{",
                            variable_end_string="}",
                        )
                        src = Path(path_template.render(conf=self.conf))
                        dest = Path("files") / self.conf["name"]
                        dest = dest / "containers" / container_id
                        dest = dest / src.relative_to(context_path)

                        if file.suffix == ".j2":
                            src_contents["templated_files"].append(
                                {
                                    "src": file.as_posix(),
                                    "dest": dest.with_suffix("").as_posix(),
                                }
                            )
                        else:
                            src_contents["files"].append(
                                {"src": file.as_posix(), "dest": dest.as_posix()}
                            )
                        src_contents["directories"].append(dest.parent.as_posix())
                        src_contents["directories"] = sorted(
                            list(set(src_contents["directories"]))
                        )

        return src_contents

    def _get_build_src_extra_files(self, src_contents):
        if self.conf_exists("extra_files"):
            for ef_id, ef_data in self.conf["extra_files"].items():
                layer = "root" if "layer" not in ef_data else ef_data["layer"]
                if "src" not in ef_data or "dest" not in ef_data:
                    continue

                src_path = Path(ef_data["src"])
                dest_path = (
                    Path(ef_data["dest"][1:])
                    if Path(ef_data["dest"]).is_absolute()
                    else Path(ef_data["dest"])
                )
                if not src_path.is_absolute():
                    src_path = Path(self.conf["base_dir"]) / src_path

                if src_path.is_file():
                    list_files = [src_path]
                elif src_path.is_dir():
                    list_files = [f for f in src_path.glob("**/*") if f.is_file()]
                else:
                    continue

                for file in list_files:
                    path_template = Template(
                        file.as_posix(),
                        variable_start_string="{",
                        variable_end_string="}",
                    )
                    src = Path(path_template.render(conf=self.conf))
                    dest = Path("files") / self.conf["name"]
                    dest = dest / "extra_files" / f"{layer}_fs"
                    dest = dest / dest_path

                    if len(list_files) == 1 and ef_data["dest"].endswith("/"):
                        dest = dest / src.name
                    else:
                        dest = dest / src.relative_to(src_path)

                    if file.suffix == ".j2":
                        if (
                            len(list_files) == 1
                            and src_path.is_file()
                            and not ef_data["dest"].endswith("/")
                        ):
                            dest = dest
                        else:
                            dest = dest.with_suffix("")
                        src_contents["templated_files"].append(
                            {
                                "src": file.as_posix(),
                                "dest": dest.as_posix(),
                            }
                        )
                    else:
                        src_contents["files"].append(
                            {"src": file.as_posix(), "dest": dest.as_posix()}
                        )
                    src_contents["directories"].append(dest.parent.as_posix())
                    src_contents["directories"] = sorted(
                        list(set(src_contents["directories"]))
                    )

    def _get_build_src_quadlets(self, src_contents):
        if self.conf_exists("containers"):
            for container_id, container_data in self.conf["containers"].items():
                layer = (
                    "root" if "layer" not in container_data else container_data["layer"]
                )
                quadlet_dest = (
                    Path("files") / self.conf["name"] / "extra_files" / f"{layer}_fs"
                )
                quadlet_dest = (
                    quadlet_dest / f"etc/containers/systemd/{container_id}.container"
                )

                quadlet_template = {
                    "src": self.conf["centos_sig"]["autosd_demo"][
                        "template_systemd_container_path"
                    ],
                    "dest": quadlet_dest.as_posix(),
                    "container_name": container_id,
                }

                src_contents["templated_files"].append(quadlet_template)
                src_contents["directories"].append(quadlet_dest.parent.as_posix())
                src_contents["directories"] = sorted(
                    list(set(src_contents["directories"]))
                )

    def _set_osbuild_config(self):
        osbuild_conf = {}
        osbuild_conf.update(self._get_osbuild_preprocessed_rpms())
        osbuild_conf.update(self._get_osbuild_preprocessed_rpm_repos())
        osbuild_conf.update(self._get_osbuild_preprocessed_containers())
        osbuild_conf.update(self._get_osbuild_preprocessed_users())
        osbuild_conf.update(self._get_osbuild_preprocessed_groups())
        osbuild_conf.update(self._get_osbuild_preprocessed_systemd())
        osbuild_conf.update(self._get_osbuild_preprocessed_extra_contents())
        self.update({"osbuild": osbuild_conf})

    def _get_osbuild_preprocessed_systemd(self):
        osbuild_conf = {"systemd": {}, "qm_systemd": {}}
        allowed_params = ["enabled_services", "disabled_services"]
        if self.conf_exists("systemd"):
            for _, systemd_data in self.conf["systemd"].items():
                systemd_v = (
                    osbuild_conf["qm_systemd"]
                    if "layer" in systemd_data and systemd_data["layer"] == "qm"
                    else osbuild_conf["systemd"]
                )
                for key, value in systemd_data.items():
                    if key in allowed_params:
                        systemd_v.setdefault(key, [])
                        if isinstance(value, list):
                            systemd_v[key] = list(set(systemd_v[key] + value))
                        elif isinstance(value, str):
                            if value not in systemd_v[key]:
                                systemd_v[key].append(value)
                        systemd_v[key].sort()

        return osbuild_conf

    def _get_osbuild_preprocessed_users(self):
        osbuild_conf = {"users": {}, "qm_users": {}}
        allowed_params = ["uid", "password", "gid", "groups", "home", "description"]
        if self.conf_exists("users"):
            for user_id, user_data in self.conf["users"].items():
                username = user_data["name"] if "name" in user_data else user_id
                new_user = {f"{username}": {}}
                for key, value in user_data.items():
                    if key in allowed_params:
                        if key == "password":
                            new_user[username][key] = sha256_crypt.hash(value)
                        elif key in ["uid", "gid"]:
                            new_user[username][key] = int(value)
                        elif key == "groups" and isinstance(value, str):
                            new_user[username][key] = [value]
                        else:
                            new_user[username][key] = value

                users_v = (
                    osbuild_conf["qm_users"]
                    if "layer" in user_data and user_data["layer"] == "qm"
                    else osbuild_conf["users"]
                )
                users_v.update(new_user)

        return osbuild_conf

    def _get_osbuild_preprocessed_groups(self):
        osbuild_conf = {"groups": {}, "qm_groups": {}}
        allowed_params = ["gid"]
        if self.conf_exists("groups"):
            for group_id, group_data in self.conf["groups"].items():
                groupname = group_data["name"] if "name" in group_data else group_id
                new_group = {f"{groupname}": {}}
                for key, value in group_data.items():
                    if key in allowed_params:
                        if key == "gid":
                            new_group[groupname][key] = int(value)

                groups_v = (
                    osbuild_conf["qm_groups"]
                    if "layer" in group_data and group_data["layer"] == "qm"
                    else osbuild_conf["groups"]
                )
                groups_v.update(new_group)

        return osbuild_conf

    def _get_osbuild_preprocessed_containers(self):
        osbuild_conf = {"containers": [], "qm_containers": []}

        if self.conf_exists("containers"):
            for container_id, container_data in self.conf["containers"].items():
                new_container = {
                    "tag": container_data.get("version", "latest"),
                    "name": container_data["systemd"]["container"]["image"],
                }
                image_name = container_data.get("image", container_id)

                if set(container_data.keys()).intersection(
                    ["container_context", "container_file"]
                ):
                    new_container["source"] = f"localhost:5000/{image_name}"
                else:
                    source = [
                        container_data.get("registry", None),
                        container_data.get("namespace", None),
                        image_name,
                    ]
                    source = [x for x in source if x is not None]
                    new_container["source"] = "/".join(source)

                if "layer" in container_data and container_data["layer"] == "qm":
                    osbuild_conf["qm_containers"].append(new_container)
                else:
                    osbuild_conf["containers"].append(new_container)

        return osbuild_conf

    def _get_osbuild_preprocessed_extra_contents(self):
        osbuild_conf = {"extra_contents": {}, "qm_extra_contents": {}}

        extra_contents_list = self["build"]["src"]["files"]
        extra_contents_list += self["build"]["src"]["templated_files"]
        extra_contents_list = sorted(extra_contents_list, key=lambda x: x["dest"])

        for item in extra_contents_list:
            dest_path = Path(item["dest"])
            if dest_path.parts[2] != "extra_files":
                continue
            layer = dest_path.parts[3].split("_")[0]

            extra_contents_v = (
                osbuild_conf["qm_extra_contents"]
                if layer == "qm"
                else osbuild_conf["extra_contents"]
            )

            item_idx = extra_contents_v.setdefault("index", 0)
            content_id = hashlib.sha256(
                f"{item['src']}\n{item['dest']}".encode()
            ).hexdigest()[:7]
            tree_dest_path = dest_path.relative_to(Path(*dest_path.parts[:4]))

            ec_input = {
                f"{layer}_extra_content_{item_idx}": {
                    "type": "org.osbuild.files",
                    "origin": "org.osbuild.source",
                    "mpp-embed": {
                        "id": f"{content_id}",
                        "path": Path("..").joinpath(dest_path).as_posix(),
                    },
                }
            }

            ec_path = {
                "from": {
                    "mpp-format-string": (
                        f"input://{layer}_extra_content_{item_idx}/{{embedded['{content_id}']}}"
                    )
                },
                "to": f"tree:///{tree_dest_path.as_posix()}",
            }

            extra_contents_v.setdefault("inputs", {}).update(ec_input)
            extra_contents_v.setdefault("paths", []).append(ec_path)
            extra_contents_v.setdefault("directories", []).append(
                "/" + tree_dest_path.parent.as_posix()
            )
            extra_contents_v["index"] += 1

        for extra_contents_v in [
            osbuild_conf["extra_contents"],
            osbuild_conf["qm_extra_contents"],
        ]:
            if "index" in extra_contents_v:
                extra_contents_v.pop("index")

            if "directories" in extra_contents_v:
                extra_contents_v["directories"] = list(
                    set(extra_contents_v["directories"])
                )
                extra_contents_v["directories"].sort()
                extra_contents_v["directories"] = [
                    {"path": d, "exist_ok": True, "parents": True}
                    for d in extra_contents_v["directories"]
                ]

        return osbuild_conf

    def _get_osbuild_preprocessed_rpm_repos(self):
        osbuild_conf = {"extra_repos": [], "qm_extra_repos": []}
        if self.conf_exists("rpm_repo"):
            for rpm_repo_id, rpm_repo_data in self.conf["rpm_repo"].items():
                if "baseurl" not in rpm_repo_data:
                    continue

                new_repo = {"id": rpm_repo_id, "baseurl": rpm_repo_data["baseurl"]}

                if "layer" in rpm_repo_data and rpm_repo_data["layer"] == "qm":
                    osbuild_conf["qm_extra_repos"].append(new_repo)
                else:
                    osbuild_conf["extra_repos"].append(new_repo)

        return osbuild_conf

    def _get_osbuild_preprocessed_rpms(self):
        osbuild_conf = {"extra_rpms": [], "qm_extra_rpms": []}

        if self.conf_exists("rpm"):
            for rpm_data in self.conf["rpm"].values():
                if "packages" not in rpm_data:
                    continue

                if "layer" in rpm_data and rpm_data["layer"] == "qm":
                    osbuild_conf["qm_extra_rpms"].extend(rpm_data["packages"])
                else:
                    osbuild_conf["extra_rpms"].extend(rpm_data["packages"])

        return osbuild_conf
