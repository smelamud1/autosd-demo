from pathlib import Path

from autosd_demo import AUTOSD_DEMO_DATA_PATH
from autosd_demo.settings import settings
from autosd_demo.utils import find_project_settings


def get_ansible_playbook_paths():
    ansible_playbooks = []
    if find_project_settings() is not None:
        ansible_playbooks_path = Path(settings.centos_sig.autosd_demo.playbooks_dir)
    else:
        ansible_playbooks_path = Path(AUTOSD_DEMO_DATA_PATH, "ansible")

    ansible_playbooks += ansible_playbooks_path.glob("*.yaml")
    ansible_playbooks += ansible_playbooks_path.glob("*.yml")

    return ansible_playbooks


def get_ansible_playbook_names():
    return [playbook.name for playbook in get_ansible_playbook_paths()]


def get_ansible_playbook_path_from_name(name):
    ansible_playbook_paths = get_ansible_playbook_paths()
    for playbook_path in ansible_playbook_paths:
        if playbook_path.name == name:
            return playbook_path
    return None
