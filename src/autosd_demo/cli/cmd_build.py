from datetime import datetime
from pathlib import Path

import click

from autosd_demo.core.executor import get_executor
from autosd_demo.utils.build import BuildEnv

from .exceptions import AnsibleError, catch_exception

PLAYBOOK_PREPARE = "build-prepare-env.yaml"


@click.command
@click.option("-H", "--host", default=None)
@click.option(
    "-d",
    "--distro",
    help="Distro used as image base to build the demo",
    default="autosd",
    show_default=True,
)
@click.option(
    "-t",
    "--target",
    help="Target used to build the demo              ",
    default="qemu",
    show_default=True,
)
@click.option(
    "-i",
    "--image-type",
    help="Image type used to build the demo          ",
    default="regular",
    show_default=True,
)
@click.option(
    "-a",
    "--image-arch",
    help="Image architecture                         ",
    default="x86_64",
    show_default=True,
)
@click.option(
    "-e",
    "--image-extension",
    help="Image extension                            ",
    default="qcow2",
    show_default=True,
)
@click.option(
    "--build-id",
    help="ID to be used for the build",
    default=f"{datetime.now():%Y%m%d_%H%M%S}",
)
@click.option(
    "--clean/--no-clean",
    is_flag=True,
    help="Clean the build folder after the build",
    default=True,
)
@click.option(
    "-v",
    "--verbose",
    is_flag=True,
    help="Increase image builder verbosity",
    default=False,
)
@catch_exception
def build(
    host,
    distro,
    target,
    image_type,
    image_arch,
    image_extension,
    build_id,
    clean,
    verbose,
):
    """Build demo"""
    build_env = BuildEnv(host, osbuild=True)

    build_env["build"].update(
        {
            "build_id": build_id,
            "distro": distro,
            "target": target,
            "image_type": image_type,
            "image_arch": image_arch,
            "image_name": (
                f"{distro}-{target}-{build_env['conf']['name']}"
                f"-{image_type}.{image_arch}.{image_extension}"
            ),
        }
    )

    executor = get_executor(remote=build_env.is_remote, become_required=True)
    with executor(host=host) as ctx:
        ctx.run_playbook("sudo-nopassword-enable.yaml", extra_vars=build_env)
        result = ctx.run_playbook(PLAYBOOK_PREPARE, extra_vars=build_env)
        if result != 0:
            raise AnsibleError(result, PLAYBOOK_PREPARE)

        src_dir = Path(build_env.build_dir, build_env.build_id, "src")
        # FIXME: Improve the failure control here
        registry_auth_path = Path(
            build_env.build_dir,
            build_env.build_id,
            "auth.json",
        )
        builder_cmd = "/usr/bin/automotive-image-builder"
        if verbose:
            builder_cmd += " --verbose"
        try:
            ctx.run(
                f"REGISTRY_AUTH_FILE=$(realpath {registry_auth_path.as_posix()}) "
                f"{builder_cmd} build --distro autosd --target {target} "
                f"--mode package --arch {image_arch} "
                f"--build-dir={src_dir.as_posix()}/_build --cache-max-size=1GB "
                f"--mpp-arg=--cache "
                f"--mpp-arg {src_dir.as_posix()}/_build/mpp_cache "
                f"--export {image_extension} "
                f"{src_dir}/images/{build_env.conf['name']}.mpp.yml "
                f"{src_dir.as_posix()}/{build_env['build']['image_name']}"
            )
            ctx.run_playbook("build-download-image.yaml", extra_vars=build_env)
        finally:
            if clean:
                ctx.run_playbook("build-clean-env.yaml", extra_vars=build_env)
            ctx.run_playbook("sudo-nopassword-disable.yaml", extra_vars=build_env)
