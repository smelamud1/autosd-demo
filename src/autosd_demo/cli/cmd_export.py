import shutil
from datetime import datetime
from pathlib import Path

import click

from autosd_demo.cli.exceptions import AnsibleError, catch_exception
from autosd_demo.core.executor import get_executor
from autosd_demo.utils.build import BuildEnv
from autosd_demo.utils.export import create_exported_tarball


@click.command()
@catch_exception
def export():
    """Export tarball for the sample-images repo"""
    build_env = BuildEnv(None, osbuild=True)
    build_env["build"].update(
        {"build_id": f"{datetime.now():%Y%m%d_%H%M%S}", "export": True}
    )
    executor = get_executor(remote=False)
    with executor(host=None) as ctx:
        result = ctx.run_playbook("build-prepare-env.yaml", extra_vars=build_env)
        if result != 0:
            raise AnsibleError(result, "build-prepare-env.yaml")

        build_path = Path(build_env.build_dir, build_env.build_id)
        create_exported_tarball(build_path)
        shutil.rmtree(build_path)
