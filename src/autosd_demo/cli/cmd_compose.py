import click

from autosd_demo.core.compose import Compose
from autosd_demo.core.executor import get_executor
from autosd_demo.utils.build import BuildEnv

from .exceptions import AnsibleError, catch_exception

SERVICES = ["registry"]
PLAYBOOK_COMPOSE = "compose-prepare.yaml"


@click.group()
def compose():
    """Run container compose commands"""


@compose.command()
@click.option("-H", "--host", default=None)
@click.argument("opts", nargs=-1)
@catch_exception
def build(host, opts):
    """Build stack images"""
    build_env = BuildEnv(host)
    run_compose = Compose(build_env.build_dir)

    executor = get_executor(remote=build_env.is_remote, become_required=False)
    with executor(host=host) as ctx:
        result = ctx.run_playbook(PLAYBOOK_COMPOSE, extra_vars=build_env)
        if result != 0:
            raise AnsibleError(result, PLAYBOOK_COMPOSE)
        cmd = run_compose.build(opts)
        click.echo(f"Running: {cmd}")
        ctx.run(cmd)


@compose.command()
@click.option("-H", "--host", default=None)
@click.argument("opts", nargs=-1)
@catch_exception
def up(host, opts):
    """Create and start the entire stack or some of its services"""
    build_env = BuildEnv(host)
    run_compose = Compose(build_env.build_dir)

    executor = get_executor(remote=build_env.is_remote, become_required=False)
    with executor(host=host) as ctx:
        result = ctx.run_playbook(PLAYBOOK_COMPOSE, extra_vars=build_env)
        if result != 0:
            raise AnsibleError(result, PLAYBOOK_COMPOSE)
        cmd = run_compose.up(opts)
        click.echo(f"Running: {cmd}")
        ctx.run(cmd)


@compose.command()
@click.option("-H", "--host", default=None)
@click.argument("opts", nargs=-1)
@catch_exception
def down(host, opts):
    """Stop the entire stack or some of its services"""
    build_env = BuildEnv(host)
    run_compose = Compose(build_env.build_dir)

    executor = get_executor(remote=build_env.is_remote, become_required=False)
    with executor(host=host) as ctx:
        result = ctx.run_playbook(PLAYBOOK_COMPOSE, extra_vars=build_env)
        if result != 0:
            raise AnsibleError(result, PLAYBOOK_COMPOSE)
        cmd = run_compose.down(opts)
        click.echo(f"Running: {cmd}")
        ctx.run(cmd)


@compose.command()
@click.option("-H", "--host", default=None)
@click.argument("opts", nargs=-1)
@catch_exception
def config(host, opts):
    """Display the resulting compose file with all included services"""
    build_env = BuildEnv(host)
    run_compose = Compose(build_env.build_dir)

    executor = get_executor(remote=build_env.is_remote, become_required=False)
    with executor(host=host) as ctx:
        result = ctx.run_playbook(PLAYBOOK_COMPOSE, extra_vars=build_env)
        if result != 0:
            raise AnsibleError(result, PLAYBOOK_COMPOSE)
        cmd = run_compose.config(opts)
        click.echo(f"Running: {cmd}")
        ctx.run(cmd)
