from pathlib import Path

import click

from autosd_demo.utils import in_autosd_demo

EXCLUDED_COMMANDS_IN_AUTOSD_PROJECT = ["new"]
EXCLUDED_COMMANDS_OUT_AUTOSD_PROJECT = ["build", "compose", "export", "show", "setup"]


class MainCLIGroup(click.Group):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def list_commands(self, ctx):
        commands = []
        for cmd_file in Path(__file__).parent.glob("cmd_*.py"):
            mod_name = cmd_file.with_suffix("").name
            cmd_name = mod_name[4:]
            commands.append(cmd_name)

        if in_autosd_demo():
            commands = list(set(commands) - set(EXCLUDED_COMMANDS_IN_AUTOSD_PROJECT))
        else:
            commands = list(set(commands) - set(EXCLUDED_COMMANDS_OUT_AUTOSD_PROJECT))

        commands.sort()
        return commands

    def get_command(self, ctx, cmd_name):
        try:
            mod = __import__(f"autosd_demo.cli.cmd_{cmd_name}", None, None, [cmd_name])
        except ImportError:  # pragma: no cover
            return None
        return getattr(mod, cmd_name)


@click.group(cls=MainCLIGroup)
def cli():
    pass
