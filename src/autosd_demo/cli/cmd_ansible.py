import click

from autosd_demo.core.executor import get_executor
from autosd_demo.utils.ansible import get_ansible_playbook_names

from .exceptions import catch_exception


@click.group(hidden=True)
@catch_exception
def ansible():  # pragma: no cover
    pass


def complete_playbooks(ctx, param, incomplete):
    playbooks = get_ansible_playbook_names()
    return [playbook for playbook in playbooks if playbook.startswith(incomplete)]


@ansible.command()
@click.option("-H", "--host", default=None)
@click.argument("playbook", shell_complete=complete_playbooks)
def play(host, playbook):
    executor = get_executor(remote=True if host else False)
    with executor(host=host) as executor:
        executor.run_playbook(playbook)


@ansible.command(name="list")
def list_playbooks():
    playbooks = get_ansible_playbook_names()
    for playbook in playbooks:
        click.echo(playbook)
