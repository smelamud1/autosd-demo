import subprocess
from contextlib import chdir
from pathlib import Path

import click
from jinja2 import Environment, FileSystemLoader

from autosd_demo import AUTOSD_DEMO_DATA_PATH

from .exceptions import FolderExists, catch_exception


@click.command()
@click.option("-n", "--name", help="name of the project")
@click.option(
    "-v",
    "--version",
    default="0.0.1",
    help="version of the project",
)
@click.option(
    "--git",
    is_flag=True,
    help="create the .git repository",
    default=False,
)
@click.argument("project_dir", required=True, type=click.Path(path_type=Path))
@catch_exception
def new(name, version, git, project_dir):
    """Create a new autosd-demo project.

    PROJECT_DIR is the name of the directory to create the project in."""
    if project_dir.exists():
        raise FolderExists(f"The folder '{project_dir}' already exists!")

    project_dir.mkdir(parents=True, exist_ok=True)

    with chdir(project_dir):
        conf_path = AUTOSD_DEMO_DATA_PATH / "conf"
        env = Environment(loader=FileSystemLoader(conf_path))
        template = env.get_template("new-project.toml.j2")

        with open(conf_path / "new-project.md") as fd:
            documentation = fd.read().splitlines()

        output = template.render(
            name=name if name else project_dir.name,
            version=version,
            documentation=documentation,
        )
        with open(".autosd-demo.toml", "w") as fd:
            fd.write(output)

        if git:
            subprocess.run("git init -b main", shell=True, check=True)
