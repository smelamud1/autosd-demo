import click

from autosd_demo.core.executor import get_executor
from autosd_demo.utils.build import BuildEnv

from .exceptions import catch_exception


@click.command
@catch_exception
@click.option("-H", "--host", default=None)
def setup(host):
    """Setup demo environment"""
    build_env = BuildEnv(host)

    executor = get_executor(remote=build_env.is_remote, become_required=True)
    with executor(host=host) as ctx:
        ctx.run_playbook("setup.yaml", extra_vars=build_env)
