from enum import Enum
from functools import wraps

import click


class FolderExists(Exception):
    pass


class AnsibleError(Exception):
    class Code(Enum):
        OK = 0
        ERROR = 1
        HOSTS_FAILED = 2
        HOSTS_UNREACHABLE = 3
        PARSER_ERROR = 4
        BAD_OPTIONS = 5
        USER_INTERRUPTED = 99
        UNEXPECTED = 250

        def __str__(self) -> str:
            return self.name.replace("_", " ").capitalize()

    def __init__(self, ec: int, playbook: str):
        self.code = self.Code(ec)
        self.playbook = playbook

    def __str__(self) -> str:
        return f'Failed to run playbook "{self.playbook}": {self.code}'


def catch_exception(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except (FolderExists, AnsibleError) as e:
            raise click.ClickException(str(e))
        except Exception as e:
            click.echo("Unexpected exception occurred!")
            raise click.ClickException(str(e))

    return wrapper
