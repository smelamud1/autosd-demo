from pathlib import Path

from dynaconf import Dynaconf

from autosd_demo import AUTOSD_DEMO_DEFAULT_CONF_PATH
from autosd_demo.utils import find_project_settings
from autosd_demo.utils.addons import get_addons_metadata


class Settings(Dynaconf):  # type: ignore[misc]
    def __init__(self):
        if find_project_settings() is not None:
            includes = [
                p.as_posix()
                for p in (AUTOSD_DEMO_DEFAULT_CONF_PATH / "includes").iterdir()
                if p.suffix in [".toml", ".py"]
            ]
            preload = [
                p.as_posix()
                for p in (AUTOSD_DEMO_DEFAULT_CONF_PATH / "preload").iterdir()
                if p.suffix in [".toml", ".py"]
            ]

            addons_includes = self._get_addons_preload_includes(includes, preload)

            preload += addons_includes

            super().__init__(**self._init_params(includes, preload))
            self._enrich_container_settings()
            self.unset("GIT_RELEASE_VERSION")
        else:
            super().__init__()

    @staticmethod
    def _init_params(includes, preload):
        return {
            "root_path": find_project_settings().parent.as_posix(),
            "includes": sorted(includes),
            "preload": sorted(preload),
            "secrets": [".secrets.toml"],
            "merge_enabled": True,
            "environments": True,
            "env_switcher": "AUTOSD_DEMO_ENV",
            "envvar_prefix": "AUTOSD_DEMO",
            "settings_files": [".autosd-demo.toml"],
        }

    @classmethod
    def _get_addons_preload_includes(cls, includes, preload):
        pre_settings = Dynaconf(**cls._init_params(includes, preload))
        addons_includes = []
        if "addons" in pre_settings:
            addons_metadata = get_addons_metadata()
            for addon_name in pre_settings["addons"]:
                if addon_name not in addons_metadata.keys():
                    continue
                addon_config = (
                    Path(addons_metadata[addon_name]["base_dir"]) / "config.toml"
                )
                addons_includes.append(str(addon_config.absolute()))
        return addons_includes

    def _enrich_container_settings(self):
        if "containers" not in self:
            return

        for container_name, container_data in self["containers"].items():
            container_defaults = [("version", "latest")]
            for key, value in container_defaults:
                if key not in container_data:
                    self["containers"][container_name][key] = value

        for container_name, container_data in self["containers"].items():
            container_systemd_defaults = [
                ("unit", "description", f"{container_name} container"),
                ("container", "container_name", container_name),
                (
                    "container",
                    "image",
                    f"localhost/{container_name}:" + container_data["version"],
                ),
                ("install", "wanted_by", "multi-user.target"),
                ("service", "restart", "always"),
            ]

            for section, key, value in container_systemd_defaults:
                if (
                    ("systemd" not in container_data)
                    or (
                        "systemd" in container_data
                        and section not in container_data["systemd"]
                    )
                    or (
                        "systemd" in container_data
                        and section in container_data["systemd"]
                        and key not in container_data["systemd"][section]
                    )
                ):
                    if "systemd" not in self["containers"][container_name]:
                        self["containers"][container_name]["systemd"] = {}
                    if section not in self["containers"][container_name]["systemd"]:
                        self["containers"][container_name]["systemd"][section] = {}
                    self["containers"][container_name]["systemd"][section][key] = value


settings = Settings()


def reload_settings():
    global settings
    settings = Settings()
