import sys


def monkeypatch_ansible_display_sys_stdout_reconfiguration():
    def _stdout_reconfigure(*args, **kwargs):  # pragma: no cover
        pass

    sys.stdout.reconfigure = _stdout_reconfigure
    sys.stderr.reconfigure = _stdout_reconfigure


def monkeypatch_ansible_display_sync():
    def _synchronize_textiowrapper(*args, **kwargs):  # pragma: no cover
        pass

    import ansible.utils.display

    ansible.utils.display.Display._synchronize_textiowrapper = (
        _synchronize_textiowrapper
    )


def monkeypatch_ansible_plugins_loader_module():
    import ansible.plugins.loader

    orig_func = ansible.plugins.loader._configure_collection_loader

    def configure_collection_loader(prefix_collections_path=None):  # pragma: no cover
        from ansible.utils.collection_loader import AnsibleCollectionConfig

        if AnsibleCollectionConfig.collection_finder:
            return
        orig_func(prefix_collections_path=prefix_collections_path)

    ansible.plugins.loader._configure_collection_loader = configure_collection_loader


def monkeypatch_ansible_context():
    import ansible.context
    from ansible.utils.context_objects import CLIArgs

    def init_global_context(cli_args):
        from ansible import context

        context.CLIARGS = CLIArgs.from_options(cli_args)

    ansible.context._init_global_context = init_global_context
