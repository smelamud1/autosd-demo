from functools import wraps

import rich
from ansible.plugins.callback import CallbackBase

DOCUMENTATION = """
    author: Roberto Majadas
    name: click_output
    type: stdout
    short_description: Show stdout output in autosd-demos
    description:
        - Show stdout output in autosd-demos
    extends_documentation_fragment:
      - default_callback
    requirements:
      - set as stdout in configuration
"""

FILTERED_TASKS = ["Gathering Facts"]


def filter_tasks(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        task = getattr(args[0], "_task", args[0])
        if self.get_task_name(task) not in FILTERED_TASKS:
            func(self, *args, **kwargs)

    return wrapper


class CallbackModule(CallbackBase):  # type: ignore[misc]
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = "stdout"
    CALLBACK_NAME = "autosd.demo.click_output"

    CALLBACK_NEEDS_ENABLED = False

    def __init__(self, status=None):
        self.status = status
        if self.status is not None:
            self.status.start()

    @staticmethod
    def get_task_name(task):
        task_name = task.get_name()
        task_name_indent = 0
        while task_name.startswith(">"):
            task_name_indent = task_name_indent + 1
            task_name = task_name.replace(">", "", 1)
        return "  " * task_name_indent + task_name

    @filter_tasks
    def v2_runner_on_failed(self, result, ignore_errors=False):
        rich.print(f"[red][■] {self.get_task_name(result._task)}[/red]")
        results = result._result.get("results", [])
        last_invocation = results[-1] if results else {}
        if last_invocation.get("failed", False):
            rich.print(f"[red]    Failed: {last_invocation['msg']}[/red]")

    @filter_tasks
    def v2_runner_on_ok(self, result):
        if result._result.get("changed", False):
            rich.print(
                "[green][[/green][yellow]🗸[/yellow][green]][/green]",
                f"[green]{self.get_task_name(result._task)}[/green]",
            )
        else:
            rich.print(f"[green][🗸] {self.get_task_name(result._task)}[/green]")

    def v2_runner_on_unreachable(self, result):  # pragma: no cover
        pass

    def v2_runner_on_skipped(self, result):  # pragma: no cover
        pass

    @filter_tasks
    def v2_playbook_on_task_start(self, task, is_conditional):  # pragma: no cover
        if self.status is not None:
            self.status.update(f"  {self.get_task_name(task)}")
