#!/usr/bin/env python3
import os
import socket
import time

import psutil

PROCESS = psutil.Process(os.getpid())
PORT = 7001

MEGA = 10**6
MEM_CHUNKS = []


class MemoryDump:
    def __init__(self, proc, tot, avail, used, free, percent) -> None:
        self.proc = proc
        self.total = tot
        self.avail = avail
        self.used = used
        self.free = free
        self.percent = percent

    def __str__(self) -> str:
        return (
            f"process = {self.proc} total = {self.total} "
            f"avail = {self.avail} used = {self.used} "
            f"free = {self.free} percent = {self.percent}"
        )

    def to_bytes(self) -> bytes:
        return self.__str__().encode()


def pmem():
    mem = psutil.virtual_memory()
    proc = PROCESS.memory_info()[1] / MEGA
    return MemoryDump(
        proc,
        mem.total / MEGA,
        mem.available / MEGA,
        mem.used / MEGA,
        mem.free / MEGA,
        mem.percent,
    )


def consume_memory(block_size):
    MEM_CHUNKS.append(" " * (block_size * MEGA))
    return pmem()


if __name__ == "__main__":
    alloc = 1
    host = socket.gethostname()
    print("Initiating:", pmem())
    print(f"Connecting to {host}:{PORT}")
    while True:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.connect((host, PORT))
            try:
                data = consume_memory(alloc)
            except MemoryError:
                MEM_CHUNKS.clear()
                alloc = 1
                continue
            else:
                alloc += 1
            print(f"Sending data (allocated {len(MEM_CHUNKS)} chunks)")
            sock.sendall(data.to_bytes())
            time.sleep(1)
