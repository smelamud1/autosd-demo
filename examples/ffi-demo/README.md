# FFI Demo

This example project demonstrates how AutoSD effectively constrains the memory
usage in the QM layer, while avoiding any interference in the Safety layer.

The example includes performance monitoring features both embedded in the image,
and as external containers that can be brought up using compose.

## Architecture

This autosd-demo project features two containerized agents that communicate
between them through the TCP port 7001, which is published to the host
on the container.

<div align="center">
    <img src="./img/diagram.png" />
</div>


### Listener agent

On the one hand, we have a listener agent that will remain actively
listening for incoming connections on the port 7001. The container resides
in the Safety layer. This agent does nothing but listening and printing
the received data. The agent shall not lose any packet, be interrupted,
or killed by the supervisor.

### Malicious agent

On the other hand, we have an agent in the QM layer that will connect to the
listener in the Safety layer, start consuming chunks of memory, and send
its latest memory consumption data through the network.

Memory consumption of this service is limited to a 50% of the available
memory.

### Monitoring

The example includes a built-in monitoring system, with some services
adding, among others:
- Advanced logging
- [Redis](https://redis.io/) server
- [Performance Co-Pilot](https://pcp.io/) API server on the port 44322
- [Grafana](https://grafana.com/) container available on the host

## Expectations

The QM agent starts eating memory faster and faster. Consequently, its
service shall get killed and restarted by the supervisor, as it will
violate the memory restriction policy. This will keep happening in cycles.

Despite this memory-hungry process running, the agent in the Safety layer
shall remain unaffected, up and running.

## Usage

### Build image
Build the image by entering the project root folder and running:

```shell
$ autosd-demo setup
$ autosd-demo build
```

Notes:
1. The image is created in the project root folder:
```
-rw-r--r--. 1 aesteve aesteve 1228079104 Apr 11 15:17 autosd-qemu-ffi-demo-regular.x86_64.qcow2
```
2. In case of remote setup & build, you still need to run `autosd-demo setup`
locally in order to install the missing prerequisites
(like `automotive-image-builder`).

### Run image

Now you can run the demo by doing:

```shell
$ automotive-image-runner --port-forward "44322:44322" autosd-qemu-ffi-demo-regular.x86_64.qcow2 &
```

QEMU window will pop up, use `root` / `password` to log in.

You can check each agent's logs:
- `ffi_server.service`
  - Available in the Safety layer.
  - We can see all the logs for data coming from the client with no
    interruption of the service or data loss.
- `ffi_client.service`
  - You need to connect to the QM layer to see this service.
    ```shell
    $ podman exec -it qm bash
    ```
  - Logs show how the service is periodically killed and restarted.
    ```
    ... localhost systemd[1]: ffi_client.service: A process of this unit has been killed by the OOM killer.
    ... localhost systemd[1]: Started ffi_client container.
    ```

### Run Grafana

Finally, we can attach the monitoring system to the running image on the host
by doing:

```shell
$ autosd-demo compose up -- -d --build
```

Verify that Grafana is running:

```shell
$ podman ps
CONTAINER ID  IMAGE                                COMMAND               CREATED       STATUS       PORTS                   NAMES
bf92ba77351f  docker.io/library/registry:latest    /etc/docker/regis...  2 days ago    Up 2 days    0.0.0.0:5000->5000/tcp  autosd_demo_local_registry
0a247e5c2891  localhost/monitoring_grafana:latest  /bin/sh -c /usr/l...  21 hours ago  Up 21 hours                          monitoring_grafana_1
```

In case of errors like `already exists` or `already in use`, try to execute:
```shell
$ autosd-demo compose down
```
and then re-run the monitoring system as described above.

Now you can just use a browser and open `localhost:3000` to get all system
data provided by the built-in monitoring add-on.
Use `admin` / `admin` to log in.

Swap memory graphics should show how the memory grows and gets emptied after
each consume cycle.

<div align="center">
    <img src="./img/screenshot.png" />
</div>
