import os
import tarfile
from pathlib import Path

import pytest


@pytest.mark.integration
def test_cli_build(simple_ws: Path):
    cfg = simple_ws.joinpath(".autosd-demo.toml")
    assert cfg.exists()
    assert os.system("autosd-demo setup") == 0

    build_cmd = (
        "autosd-demo build -e tar -v --build-id simple-ws-integration --no-clean"
    )

    assert os.system(build_cmd) == 0


@pytest.mark.integration
def test_cli_build_with_extra_files_feature(extra_files_ws: Path):
    cfg = extra_files_ws.joinpath(".autosd-demo.toml")
    assert cfg.exists()
    assert os.system("autosd-demo setup") == 0

    build_cmd = (
        "autosd-demo build -e tar -v --build-id extra-files-ws-integration --no-clean"
    )

    assert os.system(build_cmd) == 0
    assert Path("autosd-qemu-extrafiles-regular.x86_64.tar").exists()

    test_cases = [
        ("usr/lib/qm/rootfs/usr/local/share/qm_1/sample.txt", "extrafiles:qm_1:ok\n"),
        ("usr/lib/qm/rootfs/usr/local/share/qm_2/sample.txt", "extrafiles:qm_2:ok\n"),
        ("usr/lib/qm/rootfs/usr/local/share/qm_3/sample.txt", "extrafiles:qm_3:ok\n"),
        ("usr/lib/qm/rootfs/usr/local/share/qm_4/sample.txt", "extrafiles:qm_4:ok\n"),
        (
            "usr/lib/qm/rootfs/usr/local/share/qm_5/extrafiles/extrafiles.txt",
            "extrafiles:qm_5:ok\n",
        ),
        ("usr/local/share/root_1/sample.txt", "extrafiles:root_1:ok\n"),
        ("usr/local/share/root_2/sample.txt", "extrafiles:root_2:ok\n"),
        ("usr/local/share/root_3/sample.txt", "extrafiles:root_3:ok\n"),
        ("usr/local/share/root_4/sample.txt", "extrafiles:root_4:ok\n"),
        (
            "usr/local/share/root_5/extrafiles/extrafiles.txt",
            "extrafiles:root_5:ok\n",
        ),
    ]

    with tarfile.open("autosd-qemu-extrafiles-regular.x86_64.tar") as tf:
        for file, expected_content in test_cases:
            tf_file = tf.getmember(file)
            assert tf_file.isfile()
            assert tf.extractfile(file).read().decode() == expected_content
