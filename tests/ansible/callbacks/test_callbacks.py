from unittest import mock
from unittest.mock import MagicMock, call

from autosd_demo.ansible.callbacks import click_output


def test_click_output_metadata():
    cb_mod = click_output.CallbackModule()
    assert cb_mod.CALLBACK_VERSION == 2.0
    assert cb_mod.CALLBACK_TYPE == "stdout"
    assert cb_mod.CALLBACK_NAME == "autosd.demo.click_output"
    assert cb_mod.CALLBACK_NEEDS_ENABLED is False


@mock.patch("rich.print")
def test_click_output_on_failed(mock_rich_print):
    error_msg = "Test fail"
    cb_mod = click_output.CallbackModule()
    fake_result = mock.Mock()
    fake_result._result = {"results": [{"failed": True, "msg": error_msg}]}
    fake_result._task.get_name.return_value = "test_task"
    cb_mod.v2_runner_on_failed(fake_result)
    mock_rich_print.assert_has_calls(
        [call("[red][■] test_task[/red]"), call(f"[red]    Failed: {error_msg}[/red]")]
    )


@mock.patch("rich.print")
def test_click_output_ok_changed(mock_rich_print):
    cb_mod = click_output.CallbackModule()
    fake_result = mock.Mock()
    fake_result._task.get_name.return_value = "test_task"
    fake_result._result.get.return_value = True
    cb_mod.v2_runner_on_ok(fake_result)
    mock_rich_print.assert_called_once_with(
        "[green][[/green][yellow]🗸[/yellow][green]][/green]", "[green]test_task[/green]"
    )


@mock.patch("rich.print")
def test_click_output_ok_unchanged(mock_rich_print):
    cb_mod = click_output.CallbackModule()
    fake_result = mock.Mock()
    fake_result._task.get_name.return_value = "test_task"
    fake_result._result.get.return_value = False
    cb_mod.v2_runner_on_ok(fake_result)
    mock_rich_print.assert_called_once_with("[green][🗸] test_task[/green]")


def test_get_task_name_without_indent():
    cb_mod = click_output.CallbackModule()
    task = MagicMock()
    task.get_name.return_value = "Testing Task"
    assert cb_mod.get_task_name(task) == "Testing Task"


def test_get_task_name_with_indent():
    cb_mod = click_output.CallbackModule()
    task = MagicMock()
    task.get_name.return_value = ">Testing Task"
    assert cb_mod.get_task_name(task) == "  Testing Task"


@mock.patch("rich.print")
def test_filtered_task_on_ok(mock_rich_print):
    cb_mod = click_output.CallbackModule()
    fake_result = mock.Mock()
    fake_result._task.get_name.return_value = "Gathering Facts"
    fake_result._result.get.return_value = True
    cb_mod.v2_runner_on_ok(fake_result)
    assert not mock_rich_print.called


@mock.patch("rich.print")
def test_filtered_task_on_failed(mock_rich_print):
    cb_mod = click_output.CallbackModule()
    fake_result = mock.Mock()
    fake_result._task.get_name.return_value = "Gathering Facts"
    fake_result._result.get.return_value = True
    cb_mod.v2_runner_on_failed(fake_result)
    assert not mock_rich_print.called
