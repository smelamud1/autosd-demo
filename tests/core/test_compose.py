import pytest

from autosd_demo.core.compose import Compose


class ComposeDir:
    def __init__(self, tmp_dir):
        self.build_dir = tmp_dir / "build"
        self.build_dir.mkdir()
        self.path = self.build_dir / "compose"
        self.path.mkdir()
        self.user_path = self.path / "user"
        self.user_path.mkdir()

    def add_config(self, filename, conf_dir="test"):
        conf_dir = self.path / conf_dir
        conf_dir.mkdir()
        config = conf_dir / filename
        return self._touch_file(config)

    def add_user_config(self, filename):
        config = self.user_path / filename
        return self._touch_file(config)

    def _touch_file(self, path):
        if not path.is_file():
            path.touch()
        return str(path)


@pytest.fixture
def compose_dir(tmp_dir):
    yield ComposeDir(tmp_dir)


@pytest.mark.parametrize(
    "compose_config,user_config",
    [
        ("compose.yaml", None),
        (None, "compose.yml"),
        ("compose.yaml", "container-compose.yaml"),
        ("test.yaml", "user-compose.yaml"),
        ("podman-compose.yaml", "user.yaml"),
    ],
)
def test_compose_config_options(compose_dir, compose_config, user_config):
    expected_config = []
    if compose_config is not None:
        file = compose_dir.add_config(compose_config)
        if "compose" in compose_config:
            expected_config.extend(["-f", file])
    if user_config is not None:
        file = compose_dir.add_user_config(user_config)
        if "compose" in user_config:
            expected_config.extend(["-f", file])
    compose = Compose(build_dir=compose_dir.build_dir)
    assert expected_config == list(compose._config_options())


@pytest.mark.parametrize(
    "opts",
    [
        ("",),
        ("-h",),
        ("--pull",),
        ("--pull", "--no-cache"),
    ],
)
def test_compose_build(compose_dir, opts):
    # Setup compose folder
    config = "compose.yaml"
    service = "compose.service.yaml"
    config = compose_dir.add_user_config(config)
    service = compose_dir.add_config(service)
    # Init compose
    compose = Compose(build_dir=compose_dir.build_dir)
    build_cmd = compose.build(options=opts)

    expected = f"podman compose -f {service} -f {config} build"
    if any(opts):
        expected += " " + " ".join(opts)
    assert build_cmd == expected


@pytest.mark.parametrize(
    "opts",
    [
        ("",),
        ("-h",),
        ("-d",),
        ("--pull",),
        ("--pull", "--no-build"),
    ],
)
def test_compose_up(compose_dir, opts):
    # Setup compose folder
    config = "compose.yaml"
    service = "compose.service.yaml"
    config = compose_dir.add_user_config(config)
    service = compose_dir.add_config(service)
    # Init compose
    compose = Compose(build_dir=compose_dir.build_dir)
    up_cmd = compose.up(options=opts)

    expected = f"podman compose -f {service} -f {config} up"
    if any(opts):
        expected += " " + " ".join(opts)
    assert up_cmd == expected


@pytest.mark.parametrize(
    "opts",
    [
        ("",),
        ("-h",),
        ("--pull",),
    ],
)
def test_compose_down(compose_dir, opts):
    # Setup compose folder
    config = "compose.yaml"
    service = "compose.service.yaml"
    config = compose_dir.add_user_config(config)
    service = compose_dir.add_config(service)
    # Init compose
    compose = Compose(build_dir=compose_dir.build_dir)
    up_cmd = compose.down(options=opts)

    expected = f"podman compose -f {service} -f {config} down"
    if any(opts):
        expected += " " + " ".join(opts)
    assert up_cmd == expected


@pytest.mark.parametrize(
    "opts",
    [
        ("",),
        ("--services",),
    ],
)
def test_compose_config(compose_dir, opts):
    # Setup compose folder
    config = "compose.yaml"
    service = "compose.service.yaml"
    config = compose_dir.add_user_config(config)
    service = compose_dir.add_config(service)
    # Init compose
    compose = Compose(build_dir=compose_dir.build_dir)
    config_cmd = compose.config(options=opts)

    expected = f"podman compose -f {service} -f {config} config"
    if any(opts):
        expected += " " + " ".join(opts)
    assert config_cmd == expected
