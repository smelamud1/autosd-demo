from pathlib import Path
from unittest.mock import patch

from autosd_demo.utils.export import create_exported_tarball


@patch("os.system")
def test_create_exported_tarball(mock_os_system, tmp_dir):
    build_path = Path(tmp_dir) / "build" / "build_id"
    build_path.mkdir(parents=True)
    src_path = build_path.joinpath("src")
    src_path.mkdir()

    create_exported_tarball(build_path)
    tarball_path = Path(tmp_dir, "osbuild-manifests.tar.gz")
    command = f"tar czf {tarball_path.as_posix()} osbuild-manifests"
    mock_os_system.assert_called_once_with(command)
