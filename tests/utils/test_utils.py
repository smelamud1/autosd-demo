import sys
from contextlib import chdir
from pathlib import Path
from unittest.mock import MagicMock, patch

from autosd_demo import utils
from autosd_demo.utils import find_project_settings


def test_in_virtualenv_func_not_in_venv():
    with patch("sys.prefix", "/usr"):
        with patch("sys.base_prefix", "/usr"):
            assert not utils.in_virtualenv()


def test_in_virtualenv_func_in_venv():
    with patch("sys.prefix", "/usr"):
        with patch("sys.base_prefix", "/venv"):
            assert utils.in_virtualenv()


def test_in_notebook_func_when_ipython_true():
    module = type(sys)("IPython")
    module.get_ipython = MagicMock()
    module.get_ipython.return_value = type(
        "config", (), {"config": {"IPKernelApp": "exists"}}
    )

    with patch.dict("sys.modules", {"IPython": module}):
        assert utils.in_notebook()


def test_in_notebook_func_when_ipython_false():
    module = type(sys)("IPython")
    module.get_ipython = MagicMock()
    module.get_ipython.return_value = type("config", (), {"config": {}})

    with patch.dict("sys.modules", {"IPython": module}):
        assert not utils.in_notebook()


def test_in_notebook_func_when_no_ipython():
    with patch.dict("sys.modules", {"IPython": None}):
        assert not utils.in_notebook()


def test_find_project_settings(tmp_dir):
    settings_path = tmp_dir / ".autosd-demo.toml"
    settings_path.touch()
    assert find_project_settings(tmp_dir) == settings_path


def test_find_project_settings_in_a_parent_dir(tmp_dir):
    settings_path = tmp_dir / ".autosd-demo.toml"
    settings_path.touch()

    nested_dir = Path(tmp_dir) / "nested"
    nested_dir.mkdir()
    with chdir(nested_dir):
        assert find_project_settings() == settings_path


def test_find_project_settings_with_invalid_path(tmp_dir):
    invalid_path = tmp_dir.joinpath("invalid")
    assert find_project_settings(invalid_path) is None


def test_find_project_settings_without_settings(tmp_dir):
    assert find_project_settings(tmp_dir) is None


def test_find_project_settings_with_no_argument(tmp_dir):
    tmp_dir.joinpath(".autosd-demo.toml").touch()
    assert find_project_settings() == tmp_dir / ".autosd-demo.toml"
