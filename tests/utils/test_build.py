from pathlib import Path
from unittest.mock import patch

import pytest
from passlib.handlers.sha2_crypt import sha256_crypt

from autosd_demo.utils.build import BuildEnv


class TestBuildEnv(BuildEnv):
    def __init__(self, osbuild=False):
        super(BuildEnv, self).__init__()

        self.update(
            {
                "build": {
                    "src": {
                        "directories": [],
                        "files": [],
                        "templated_files": [],
                        "fetch_files": [],
                    }
                }
            }
        )

        if osbuild:
            self._set_osbuild_config()


@patch("autosd_demo.utils.build.settings")
def test_get_build_env_config_with_local_env(mock_settings):
    mock_settings.as_dict.return_value = {
        "base_dir": "~/src",
        "build_dir": "~/src/build",
    }
    build_env = BuildEnv(host=None)
    assert build_env["conf"]["base_dir"] == "~/src"
    assert build_env["conf"]["build_dir"] == "~/src/build"
    assert build_env["build"]["base_dir"] == "~/src"
    assert build_env["build"]["build_dir"] == "~/src/build"
    assert build_env["build"]["is_remote"] is False


@pytest.mark.parametrize(
    "settings_data, expected",
    [
        (
            {
                "remote_host": {
                    "foo": {"base_dir": "/home/foo", "build_dir": "/home/foo/build"}
                }
            },
            {"base_dir": "/home/foo", "build_dir": "/home/foo/build"},
        ),
        (
            {
                "remote_host": {
                    "foo": {
                        "base_dir": "/home/foo",
                    }
                }
            },
            {"base_dir": "/home/foo", "build_dir": "/home/foo/build"},
        ),
        ({"remote_host": {"foo": {}}}, {"base_dir": "~", "build_dir": "~/build"}),
        ({}, {"base_dir": "~", "build_dir": "~/build"}),
    ],
)
@patch("autosd_demo.utils.build.settings")
def test_get_build_env_config_with_remote_env(mock_settings, settings_data, expected):
    mock_settings.as_dict.return_value = settings_data
    result = BuildEnv(host="foo")
    assert "conf" in result
    assert "build" in result
    assert result["build"]["base_dir"] == expected["base_dir"]
    assert result["build"]["build_dir"] == expected["build_dir"]


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf, expected",
    [
        ({}, {"systemd": {}, "qm_systemd": {}}),
        (
            {"systemd": {"service1": {"enabled_services": "ssh"}}},
            {"systemd": {"enabled_services": ["ssh"]}, "qm_systemd": {}},
        ),
        (
            {"systemd": {"service1": {"layer": "qm", "enabled_services": "ssh"}}},
            {"systemd": {}, "qm_systemd": {"enabled_services": ["ssh"]}},
        ),
        (
            {"systemd": {"service1": {"layer": "qm", "not_allowed_key": "value"}}},
            {"systemd": {}, "qm_systemd": {}},
        ),
        (
            {
                "systemd": {
                    "service1": {"layer": "qm", "enabled_services": ["ssh", "httpd"]}
                }
            },
            {"systemd": {}, "qm_systemd": {"enabled_services": ["httpd", "ssh"]}},
        ),
        (
            {
                "systemd": {
                    "service1": {
                        "layer": "qm",
                        "enabled_services": ["ssh", "httpd", "ssh"],
                    }
                }
            },
            {"systemd": {}, "qm_systemd": {"enabled_services": ["httpd", "ssh"]}},
        ),
        (
            {
                "systemd": {
                    "service1": {"enabled_services": "ssh"},
                    "service2": {"enabled_services": "ssh"},
                }
            },
            {"systemd": {"enabled_services": ["ssh"]}, "qm_systemd": {}},
        ),
    ],
)
def test_get_osbuild_preprocessed_systemd(mock_settings, conf, expected):
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    result = test._get_osbuild_preprocessed_systemd()
    assert result == expected


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf, expected_result",
    [
        ({}, {"users": {}, "qm_users": {}}),
        (
            {
                "users": {
                    "user1": {
                        "name": "user1",
                        "gid": "1000",
                        "uid": "1000",
                        "groups": "admin",
                        "home": "/home/user1",
                    }
                }
            },
            {
                "users": {
                    "user1": {
                        "gid": 1000,
                        "uid": 1000,
                        "groups": ["admin"],
                        "home": "/home/user1",
                    }
                },
                "qm_users": {},
            },
        ),
        (
            {
                "users": {
                    "user1": {
                        "name": "user1",
                        "gid": 1000,
                        "uid": 1000,
                        "groups": ["admin"],
                        "home": "/home/user1",
                        "layer": "qm",
                    }
                }
            },
            {
                "users": {},
                "qm_users": {
                    "user1": {
                        "gid": 1000,
                        "uid": 1000,
                        "groups": ["admin"],
                        "home": "/home/user1",
                    }
                },
            },
        ),
    ],
)
def test_get_osbuild_preprocessed_users(mock_settings, conf, expected_result):
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    result = test._get_osbuild_preprocessed_users()
    assert result == expected_result


@patch("autosd_demo.utils.build.settings")
def test_get_osbuild_preprocessed_users_with_password(mock_settings):
    conf = {"users": {"user1": {"name": "user1", "password": "password1234"}}}
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    result = test._get_osbuild_preprocessed_users()
    assert sha256_crypt.verify("password1234", result["users"]["user1"]["password"])


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf, expected_result",
    [
        ({}, {"groups": {}, "qm_groups": {}}),
        (
            {"groups": {"group1": {"gid": "1020"}}},
            {"groups": {"group1": {"gid": 1020}}, "qm_groups": {}},
        ),
        (
            {"groups": {"group1": {"gid": "1020", "layer": "qm"}}},
            {"groups": {}, "qm_groups": {"group1": {"gid": 1020}}},
        ),
    ],
)
def test_get_osbuild_preprocessed_groups(mock_settings, conf, expected_result):
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    result = test._get_osbuild_preprocessed_groups()
    assert result == expected_result


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf,expected_result",
    [
        ({}, {"containers": [], "qm_containers": []}),
        (
            {
                "containers": {
                    "app": {
                        "systemd": {"container": {"image": "localhost/app"}},
                    },
                    "qm_app": {
                        "systemd": {"container": {"image": "localhost/qm_app"}},
                        "layer": "qm",
                    },
                }
            },
            {
                "containers": [
                    {"tag": "latest", "name": "localhost/app", "source": "app"}
                ],
                "qm_containers": [
                    {"tag": "latest", "name": "localhost/qm_app", "source": "qm_app"}
                ],
            },
        ),
        (
            {
                "containers": {
                    "app": {
                        "registry": "quay.io",
                        "image": "python",
                        "version": "3",
                        "systemd": {"container": {"image": "localhost/app"}},
                    },
                    "qm_app": {
                        "registry": "quay.io",
                        "image": "python",
                        "version": "3",
                        "systemd": {"container": {"image": "localhost/qm_app"}},
                        "layer": "qm",
                    },
                }
            },
            {
                "containers": [
                    {"tag": "3", "name": "localhost/app", "source": "quay.io/python"}
                ],
                "qm_containers": [
                    {"tag": "3", "name": "localhost/qm_app", "source": "quay.io/python"}
                ],
            },
        ),
        (
            {
                "containers": {
                    "app": {
                        "image": "python",
                        "container_context": "containers/app",
                        "version": "3",
                        "systemd": {"container": {"image": "localhost/app"}},
                    },
                    "qm_app": {
                        "image": "python",
                        "container_context": "containers/app",
                        "version": "3",
                        "systemd": {"container": {"image": "localhost/qm_app"}},
                        "layer": "qm",
                    },
                }
            },
            {
                "containers": [
                    {
                        "tag": "3",
                        "name": "localhost/app",
                        "source": "localhost:5000/python",
                    }
                ],
                "qm_containers": [
                    {
                        "tag": "3",
                        "name": "localhost/qm_app",
                        "source": "localhost:5000/python",
                    }
                ],
            },
        ),
    ],
)
def test_get_osbuild_preprocessed_containers(mock_settings, conf, expected_result):
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    result = test._get_osbuild_preprocessed_containers()
    assert result == expected_result


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf,expected_result",
    [
        (
            {"rpm_repo": {"repo1": {"baseurl": "http://example.com", "layer": "qm"}}},
            {
                "extra_repos": [],
                "qm_extra_repos": [{"id": "repo1", "baseurl": "http://example.com"}],
            },
        ),
        (
            {
                "rpm_repo": {
                    "repo1": {
                        "baseurl": "http://example.com",
                    }
                }
            },
            {
                "extra_repos": [{"id": "repo1", "baseurl": "http://example.com"}],
                "qm_extra_repos": [],
            },
        ),
        (
            {"rpm_repo": {"repo1": {"layer": "qm"}}},
            {"extra_repos": [], "qm_extra_repos": []},
        ),
        ({}, {"extra_repos": [], "qm_extra_repos": []}),
    ],
)
def test_get_osbuild_preprocessed_rpm_repos(mock_settings, conf, expected_result):
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    result = test._get_osbuild_preprocessed_rpm_repos()
    assert result == expected_result


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf, expected_result",
    [
        (
            {
                "rpm": {
                    "rpm1": {"packages": ["pkg1", "pkg2"], "layer": "qm"},
                    "rpm2": {
                        "packages": ["pkg3", "pkg4"],
                    },
                    "rpm3": {"no_packages_field": True},
                }
            },
            {
                "extra_rpms": ["pkg3", "pkg4"],
                "qm_extra_rpms": ["pkg1", "pkg2"],
            },
        ),
        (
            {
                "rpm": {
                    "rpm1": {"no_packages_field": True},
                }
            },
            {"extra_rpms": [], "qm_extra_rpms": []},
        ),
        ({}, {"extra_rpms": [], "qm_extra_rpms": []}),
    ],
)
def test_get_osbuild_preprocessed_rpms(mock_settings, conf, expected_result):
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    result = test._get_osbuild_preprocessed_rpms()
    assert result == expected_result


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf, build_src_conf, expected",
    [
        (
            {},
            {"files": [], "templated_files": []},
            {"extra_contents": {}, "qm_extra_contents": {}},
        ),
        (
            {},
            {
                "directories": [
                    "files/mydemo/containers/alpha",
                    "files/mydemo/containers/alpha/conf",
                ],
                "files": [
                    {
                        "dest": "files/mydemo/containers/alpha/Containerfile",
                        "src": "containers/alpha/Containerfile",
                    },
                    {
                        "dest": "files/mydemo/containers/alpha/app.py",
                        "src": "containers/alpha/app.py",
                    },
                ],
                "templated_files": [
                    {
                        "dest": "files/mydemo/containers/alpha/conf/config.yaml",
                        "src": "containers/alpha/conf/config.yaml.j2",
                    }
                ],
            },
            {"extra_contents": {}, "qm_extra_contents": {}},
        ),
        (
            {},
            {
                "directories": [
                    "files/mydemo/extra_files/qm_fs/etc/containers/systemd",
                    "files/mydemo/extra_files/root_fs/etc/containers/systemd",
                ],
                "files": [],
                "templated_files": [
                    {
                        "dest": (
                            "files/mydemo/extra_files/root_fs/"
                            "etc/containers/systemd/alpha.container"
                        ),
                        "src": "/test/systemd.j2",
                    },
                    {
                        "dest": (
                            "files/mydemo/extra_files/qm_fs/"
                            "etc/containers/systemd/beta.container"
                        ),
                        "src": "/test/systemd.j2",
                    },
                ],
                "fetch_files": [],
            },
            {
                "extra_contents": {
                    "directories": [
                        {
                            "exist_ok": True,
                            "parents": True,
                            "path": "/etc/containers/systemd",
                        }
                    ],
                    "inputs": {
                        "root_extra_content_0": {
                            "mpp-embed": {
                                "id": "113b38d",
                                "path": "../files/mydemo/extra_files/root_fs/"
                                "etc/containers/systemd/alpha.container",
                            },
                            "origin": "org.osbuild.source",
                            "type": "org.osbuild.files",
                        }
                    },
                    "paths": [
                        {
                            "from": {
                                "mpp-format-string": "input://root_extra_content_0/{embedded['113b38d']}"
                            },
                            "to": "tree:///etc/containers/systemd/alpha.container",
                        }
                    ],
                },
                "qm_extra_contents": {
                    "directories": [
                        {
                            "exist_ok": True,
                            "parents": True,
                            "path": "/etc/containers/systemd",
                        }
                    ],
                    "inputs": {
                        "qm_extra_content_0": {
                            "mpp-embed": {
                                "id": "b1481e0",
                                "path": "../files/mydemo/extra_files/qm_fs/"
                                "etc/containers/systemd/beta.container",
                            },
                            "origin": "org.osbuild.source",
                            "type": "org.osbuild.files",
                        }
                    },
                    "paths": [
                        {
                            "from": {
                                "mpp-format-string": "input://qm_extra_content_0/{embedded['b1481e0']}"
                            },
                            "to": "tree:///etc/containers/systemd/beta.container",
                        }
                    ],
                },
            },
        ),
    ],
)
def test_get_osbuild_preprocessed_extra_contents(
    mock_settings, conf, build_src_conf, expected
):
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    test["build"]["src"] = build_src_conf
    result = test._get_osbuild_preprocessed_extra_contents()
    assert result == expected


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf, expected_result",
    [
        (
            {},
            {"directories": [], "files": [], "templated_files": [], "fetch_files": []},
        ),
        (
            {
                "name": "mydemo",
                "centos_sig": {
                    "autosd_demo": {
                        "template_systemd_container_path": "/test/systemd.j2"
                    }
                },
                "containers": {"alpha": {}, "beta": {"layer": "qm"}},
            },
            {
                "directories": [
                    "files/mydemo/extra_files/qm_fs/etc/containers/systemd",
                    "files/mydemo/extra_files/root_fs/etc/containers/systemd",
                ],
                "files": [],
                "templated_files": [
                    {
                        "container_name": "alpha",
                        "dest": (
                            "files/mydemo/extra_files/root_fs/"
                            "etc/containers/systemd/alpha.container"
                        ),
                        "src": "/test/systemd.j2",
                    },
                    {
                        "container_name": "beta",
                        "dest": (
                            "files/mydemo/extra_files/qm_fs/"
                            "etc/containers/systemd/beta.container"
                        ),
                        "src": "/test/systemd.j2",
                    },
                ],
                "fetch_files": [],
            },
        ),
    ],
)
def test_get_build_src_quadlets(mock_settings, conf, expected_result):
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    src_contents = {
        "directories": [],
        "files": [],
        "templated_files": [],
        "fetch_files": [],
    }
    test._get_build_src_quadlets(src_contents)
    assert src_contents == expected_result


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf, extra_files, expected_result",
    [
        (
            {},
            [],
            {"directories": [], "files": [], "templated_files": [], "fetch_files": []},
        ),
        (
            {
                "name": "mydemo",
                "containers": {"alpha": {"container_context": "containers/alpha"}},
            },
            [
                "containers/alpha/Containerfile",
                "containers/alpha/app.py",
                "containers/alpha/conf/config.yaml.j2",
            ],
            {
                "directories": [
                    "files/mydemo/containers/alpha",
                    "files/mydemo/containers/alpha/conf",
                ],
                "files": [
                    {
                        "dest": "files/mydemo/containers/alpha/Containerfile",
                        "src": "containers/alpha/Containerfile",
                    },
                    {
                        "dest": "files/mydemo/containers/alpha/app.py",
                        "src": "containers/alpha/app.py",
                    },
                ],
                "templated_files": [
                    {
                        "dest": "files/mydemo/containers/alpha/conf/config.yaml",
                        "src": "containers/alpha/conf/config.yaml.j2",
                    }
                ],
                "fetch_files": [],
            },
        ),
    ],
)
def test_get_build_src_custom_containers_files(
    mock_settings, conf, extra_files, expected_result, tmp_dir
):
    def sort_container_files(data):
        return {
            key: sorted(value, key=lambda d: d["src"] if isinstance(d, dict) else d)
            for key, value in sorted(data.items())
        }

    conf.update({"base_dir": "."})
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    src_contents = {
        "directories": [],
        "files": [],
        "templated_files": [],
        "fetch_files": [],
    }

    for extra_file in extra_files:
        extra_file_path = tmp_dir / Path(extra_file)
        extra_file_path.parent.mkdir(parents=True, exist_ok=True)
        extra_file_path.touch()

    test._get_build_src_custom_containers_files(src_contents)
    assert sort_container_files(src_contents) == sort_container_files(expected_result)


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "conf, extra_files, expected_result",
    [
        (
            {},
            [],
            {"directories": [], "files": [], "templated_files": [], "fetch_files": []},
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test": {"src": "data/assets", "dest": "usr/share/demo"}
                },
            },
            [
                "data/assets/text1.txt",
                "data/assets/text2.txt.j2",
            ],
            {
                "directories": ["files/mydemo/extra_files/root_fs/usr/share/demo"],
                "files": [
                    {
                        "dest": (
                            "files/mydemo/extra_files/root_fs/"
                            "usr/share/demo/text1.txt"
                        ),
                        "src": "data/assets/text1.txt",
                    }
                ],
                "templated_files": [
                    {
                        "dest": (
                            "files/mydemo/extra_files/root_fs/"
                            "usr/share/demo/text2.txt"
                        ),
                        "src": "data/assets/text2.txt.j2",
                    }
                ],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "src": "data/assets/text1.txt",
                        "dest": "usr/share/demo/",
                        "layer": "qm",
                    },
                    "test2": {
                        "src": "data/assets/text1.txt",
                        "dest": "usr/share/text1.txt",
                        "layer": "qm",
                    },
                },
            },
            ["data/assets/text1.txt"],
            {
                "directories": [
                    "files/mydemo/extra_files/qm_fs/usr/share",
                    "files/mydemo/extra_files/qm_fs/usr/share/demo",
                ],
                "files": [
                    {
                        "dest": (
                            "files/mydemo/extra_files/qm_fs/usr/share/demo/text1.txt"
                        ),
                        "src": "data/assets/text1.txt",
                    },
                    {
                        "dest": "files/mydemo/extra_files/qm_fs/usr/share/text1.txt",
                        "src": "data/assets/text1.txt",
                    },
                ],
                "templated_files": [],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {"src": "data/assets/text1.txt"},
                },
            },
            [],
            {"directories": [], "files": [], "templated_files": [], "fetch_files": []},
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {"src": "data/assets/text1.txt", "dest": "usr/share/demo/"}
                },
            },
            [],
            {"directories": [], "files": [], "templated_files": [], "fetch_files": []},
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "src": "data/assets/text1.txt.j2",
                        "dest": "usr/share/demo/",
                    }
                },
            },
            ["data/assets/text1.txt.j2"],
            {
                "directories": ["files/mydemo/extra_files/root_fs/usr/share/demo"],
                "files": [],
                "templated_files": [
                    {
                        "dest": (
                            "files/mydemo/extra_files/root_fs/"
                            "usr/share/demo/text1.txt"
                        ),
                        "src": "data/assets/text1.txt.j2",
                    }
                ],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "src": "data/assets/text1.txt.j2",
                        "dest": "usr/share/demo/test.txt",
                    }
                },
            },
            ["data/assets/text1.txt.j2"],
            {
                "directories": ["files/mydemo/extra_files/root_fs/usr/share/demo"],
                "files": [],
                "templated_files": [
                    {
                        "dest": (
                            "files/mydemo/extra_files/root_fs/"
                            "usr/share/demo/test.txt"
                        ),
                        "src": "data/assets/text1.txt.j2",
                    }
                ],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {"src": "data/assets", "dest": "usr/share/demo"}
                },
            },
            ["data/assets/subfolder/text1.txt.j2"],
            {
                "directories": [
                    "files/mydemo/extra_files/root_fs/usr/share/demo/subfolder"
                ],
                "files": [],
                "templated_files": [
                    {
                        "dest": (
                            "files/mydemo/extra_files/root_fs/"
                            "usr/share/demo/subfolder/text1.txt"
                        ),
                        "src": "data/assets/subfolder/text1.txt.j2",
                    }
                ],
                "fetch_files": [],
            },
        ),
    ],
)
def test_get_build_src_extra_files(
    mock_settings, conf, extra_files, expected_result, tmp_dir
):
    conf.update({"base_dir": "."})
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    src_contents = {
        "directories": [],
        "files": [],
        "templated_files": [],
        "fetch_files": [],
    }

    for extra_file in extra_files:
        extra_file_path = tmp_dir / Path(extra_file)
        extra_file_path.parent.mkdir(parents=True, exist_ok=True)
        extra_file_path.touch()

    test._get_build_src_extra_files(src_contents)
    assert src_contents == expected_result


@patch("autosd_demo.utils.build.settings")
@pytest.mark.parametrize(
    "directory_list, expected_result",
    [
        (
            ["/usr/local", "/usr/local/share", "/usr/local/share/demo"],
            ["/usr/local/share/demo"],
        ),
        (
            ["/usr/local", "/usr/local/share/demo", "/usr/share/demo"],
            ["/usr/local/share/demo", "/usr/share/demo"],
        ),
    ],
)
def test_reduce_build_src_directory_list(
    mock_settings, directory_list, expected_result
):
    mock_settings.as_dict.return_value = {}
    test = TestBuildEnv()
    src_contents = {
        "directories": directory_list,
        "files": [],
        "templated_files": [],
        "fetch_files": [],
    }
    test._reduce_build_src_directory_list(src_contents)
    assert src_contents["directories"] == expected_result


@patch("src.autosd_demo.utils.build.settings")
def test_get_preprocessed_osbuild_config(mock_settings):
    mock_settings.as_dict.return_value = {}
    expected_osbuild_conf = {
        "build": {
            "src": {
                "directories": [],
                "files": [],
                "templated_files": [],
                "fetch_files": [],
            }
        },
        "osbuild": {
            "containers": [],
            "extra_contents": {},
            "extra_repos": [],
            "extra_rpms": [],
            "groups": {},
            "qm_containers": [],
            "qm_extra_contents": {},
            "qm_extra_repos": [],
            "qm_extra_rpms": [],
            "qm_groups": {},
            "qm_systemd": {},
            "qm_users": {},
            "systemd": {},
            "users": {},
        },
    }

    actual_osbuild_conf = TestBuildEnv(osbuild=True)

    assert actual_osbuild_conf == expected_osbuild_conf
