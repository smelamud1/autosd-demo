from unittest.mock import patch

from autosd_demo.utils.addons import get_addons_metadata


def test_get_addons_metadata(tmp_dir):
    with patch("autosd_demo.utils.addons.AUTOSD_DEMO_DATA_PATH", tmp_dir):
        (tmp_dir / "addons" / "addon1").mkdir(parents=True)
        (tmp_dir / "addons" / "addon1" / "config.toml").touch(exist_ok=True)
        (tmp_dir / "addons" / "addon2").mkdir(parents=True)
        (tmp_dir / "addons" / "addon2" / "config.toml").touch(exist_ok=True)
        (tmp_dir / "addons" / "addon3").mkdir(parents=True)
        (tmp_dir / "addons" / "addon3" / "config.toml").touch(exist_ok=True)
        (tmp_dir / "addons" / "addon4").mkdir(parents=True)

        metadata = get_addons_metadata()
        for addon in ["addon1", "addon2", "addon3"]:
            assert addon in metadata
            expected_addon_path = (tmp_dir / "addons" / addon).absolute()
            assert metadata[addon]["base_dir"] == str(expected_addon_path)

        assert "addon4" not in metadata
