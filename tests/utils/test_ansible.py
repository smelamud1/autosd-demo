import importlib
from pathlib import Path

import autosd_demo.utils.ansible
from autosd_demo import AUTOSD_DEMO_DATA_PATH, settings
from autosd_demo.utils.ansible import (
    get_ansible_playbook_names,
    get_ansible_playbook_path_from_name,
    get_ansible_playbook_paths,
)


def test_ansible_utils_get_ansible_playbook_paths():
    assert (
        Path(AUTOSD_DEMO_DATA_PATH, "ansible", "test.yaml")
        in get_ansible_playbook_paths()
    )


def test_ansible_utils_get_ansible_playbook_paths_in_project(tmp_dir):
    settings_path = tmp_dir / ".autosd-demo.toml"
    settings_path.touch()

    settings.reload_settings()
    importlib.reload(autosd_demo.utils.ansible)

    test_playbook_path = Path(AUTOSD_DEMO_DATA_PATH, "ansible", "test.yaml")
    assert test_playbook_path in get_ansible_playbook_paths()


def test_ansible_utils_get_ansible_playbook_names():
    assert "test.yaml" in get_ansible_playbook_names()


def test_ansible_utils_get_ansible_playbook_path_from_name():
    playbook_name = "test.yaml"
    playbook_path = Path(AUTOSD_DEMO_DATA_PATH, "ansible", playbook_name)
    assert get_ansible_playbook_path_from_name(playbook_name) == playbook_path
    assert get_ansible_playbook_path_from_name("fake.yml") is None
