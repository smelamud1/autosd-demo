import json
import tomllib
from unittest.mock import MagicMock, mock_open, patch

import yaml
from click.testing import CliRunner

from autosd_demo import settings
from autosd_demo.cli import cli


def test_cmd_show_config(tmp_dir):
    settings_path = tmp_dir / ".autosd-demo.toml"
    settings_path.touch()

    settings.reload_settings()
    runner = CliRunner()

    result = runner.invoke(cli, ["show", "config"])
    assert result.exit_code == 0
    assert tomllib.loads(result.output)["base_dir"] == tmp_dir.as_posix()

    result = runner.invoke(cli, ["show", "config", "-o", "json"])
    assert result.exit_code == 0
    assert json.loads(result.output)["base_dir"] == tmp_dir.as_posix()

    result = runner.invoke(cli, ["show", "config", "-o", "yaml"])
    assert result.exit_code == 0
    assert yaml.safe_load(result.output)["base_dir"] == tmp_dir.as_posix()


@patch("builtins.open", new_callable=mock_open, read_data="data")
@patch("autosd_demo.cli.cmd_show.BuildEnv")
@patch("autosd_demo.cli.cmd_show.get_executor")
def test_cmd_show_osbuild_manifest(mock_executor, mock_build_env, mock_manifest_file):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    runner.invoke(cli, ["show", "osbuild-manifest"])

    mock_run_playbook.assert_called_once()
    mock_manifest_file.assert_called_once()
    mock_build_env.assert_called_once_with(None, osbuild=True)
