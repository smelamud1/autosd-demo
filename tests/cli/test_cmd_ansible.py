import importlib
from unittest.mock import MagicMock, patch

from click.testing import CliRunner

import autosd_demo
import autosd_demo.utils.build
from autosd_demo import settings
from autosd_demo.cli import cli
from autosd_demo.cli.cmd_ansible import complete_playbooks


def test_ansible_cmd_list(tmp_dir):
    settings_path = tmp_dir / ".autosd-demo.toml"
    settings_path.touch()

    runner = CliRunner()

    settings.reload_settings()
    importlib.reload(autosd_demo.cli)
    importlib.reload(autosd_demo.utils.ansible)

    result = runner.invoke(cli, ["ansible", "list"])
    assert "test.yaml" in result.stdout.strip()
    assert result.exit_code == 0


@patch("autosd_demo.cli.cmd_ansible.get_executor")
def test_ansible_cmd_play(mock_executor, tmp_dir):
    settings_path = tmp_dir / ".autosd-demo.toml"
    settings_path.touch()

    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    result = runner.invoke(cli, ["ansible", "play", "test.yaml"])

    assert result.exit_code == 0
    mock_executor.assert_called_once_with(remote=False)
    mock_executor_ctx.assert_called_once_with(host=None)
    mock_run_playbook.assert_called_once_with("test.yaml")


def test_ansible_cmd_play_with_not_existing_playbook():
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(cli, ["ansible", "play", "fake.yaml"])
        assert result.exit_code == 1


def test_ansible_cmd_complete_playbooks_func():
    with patch(
        "autosd_demo.cli.cmd_ansible.get_ansible_playbook_names"
    ) as mock_get_playbooks:
        mock_get_playbooks.return_value = [
            "playbook1.yml",
            "playbook2.yml",
            "playbook3.yml",
        ]

        # Test case 1: Incomplete starts with 'play'
        result = complete_playbooks(None, None, "play")
        assert result == ["playbook1.yml", "playbook2.yml", "playbook3.yml"]

        # Test case 2: Incomplete is blank
        result = complete_playbooks(None, None, "")
        assert result == ["playbook1.yml", "playbook2.yml", "playbook3.yml"]

        # Test case 3: Incomplete starts with 'playbook1'
        result = complete_playbooks(None, None, "playbook1")
        assert result == ["playbook1.yml"]

        # Test case 4: Incomplete starts with 'no_match'
        result = complete_playbooks(None, None, "no_match")
        assert result == []
