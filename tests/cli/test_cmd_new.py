import subprocess

from click.testing import CliRunner

from autosd_demo.cli import cli


def test_cmd_new(tmp_dir):
    runner = CliRunner()
    result = runner.invoke(cli, ["new", "new_project"])
    assert result.exit_code == 0

    new_project_dir = tmp_dir.joinpath("new_project")
    assert new_project_dir.is_dir()
    assert new_project_dir.joinpath(".autosd-demo.toml").is_file()
    assert not new_project_dir.joinpath(".git").is_dir()


def test_cmd_new_with_git(tmp_dir):
    runner = CliRunner()
    result = runner.invoke(cli, ["new", "new_project", "--git"])
    assert result.exit_code == 0

    new_project_dir = tmp_dir.joinpath("new_project")
    assert new_project_dir.is_dir()
    assert new_project_dir.joinpath(".autosd-demo.toml").is_file()

    assert new_project_dir.joinpath(".git").is_dir()
    proc = subprocess.run(
        "git branch --show-current",
        cwd=new_project_dir,
        stdout=subprocess.PIPE,
        text=True,
        shell=True,
        check=True,
    )
    assert proc.stdout.strip("\n") == "main"


def test_cmd_new_project_already_exists(tmp_dir):
    new_project = tmp_dir / "new_project"
    new_project.mkdir()

    runner = CliRunner()
    result = runner.invoke(cli, ["new", "new_project"])
    assert result.exit_code == 1
    assert "already exists" in result.output
