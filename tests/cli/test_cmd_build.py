from unittest.mock import MagicMock, patch

from click.testing import CliRunner

from autosd_demo.cli import cli


class AnyStringWith(str):
    def __eq__(self, other):
        return self in other


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
def test_build_with_local_env(mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = False

    runner = CliRunner()
    runner.invoke(cli, ["build"])

    mock_build_env.assert_called_once_with(None, osbuild=True)
    mock_executor.assert_called_once_with(remote=False, become_required=True)
    mock_executor_ctx.assert_called_once_with(host=None)
    mock_run_playbook.assert_any_call(
        "sudo-nopassword-enable.yaml", extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-prepare-env.yaml", extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-download-image.yaml", extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-clean-env.yaml", extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        "sudo-nopassword-disable.yaml",
        extra_vars=mock_build_env.return_value,
    )
    assert mock_run_playbook.call_count == 5


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
def test_build_prepare_error(mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=1)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    result = runner.invoke(cli, ["build"])

    mock_build_env.assert_called_once_with(None, osbuild=True)
    mock_executor_ctx.assert_called_once_with(host=None)
    mock_run_playbook.assert_any_call(
        "sudo-nopassword-enable.yaml", extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-prepare-env.yaml", extra_vars=mock_build_env.return_value
    )
    assert mock_run_playbook.call_count == 2
    assert result.exit_code == 1
    assert "Failed to run playbook" in result.output


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
def test_setup_with_remote_env(mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = True

    runner = CliRunner()
    runner.invoke(cli, ["build", "-H", "foobar"])

    mock_executor.assert_called_once_with(remote=True, become_required=True)
    mock_executor_ctx.assert_called_once_with(host="foobar")
    mock_run_playbook.assert_any_call(
        "sudo-nopassword-enable.yaml", extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-prepare-env.yaml", extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-download-image.yaml", extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-clean-env.yaml", extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        "sudo-nopassword-disable.yaml",
        extra_vars=mock_build_env.return_value,
    )
    assert mock_run_playbook.call_count == 5


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
def test_build_with_local_env_and_no_clean(mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = False

    runner = CliRunner()
    runner.invoke(cli, ["build", "--no-clean"])

    mock_build_env.assert_called_once_with(None, osbuild=True)
    mock_executor.assert_called_once_with(remote=False, become_required=True)
    mock_executor_ctx.assert_called_once_with(host=None)
    mock_run_playbook.assert_any_call(
        "sudo-nopassword-enable.yaml", extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-prepare-env.yaml", extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-download-image.yaml", extra_vars=mock_build_env.return_value
    )
    mock_run_playbook.assert_any_call(
        "sudo-nopassword-disable.yaml",
        extra_vars=mock_build_env.return_value,
    )
    assert mock_run_playbook.call_count == 4


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
def test_build_with_local_env_and_verbose(mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run = MagicMock(return_value=0)
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor_ctx.return_value.__enter__.return_value.run = mock_run
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = False

    runner = CliRunner()
    runner.invoke(cli, ["build", "--verbose"])

    mock_build_env.assert_called_once_with(None, osbuild=True)
    mock_executor.assert_called_once_with(remote=False, become_required=True)
    mock_executor_ctx.assert_called_once_with(host=None)
    mock_run.assert_called_once_with(AnyStringWith("--verbose"))
    assert mock_run.call_count == 1
