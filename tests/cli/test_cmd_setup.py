from unittest.mock import MagicMock, patch

from click.testing import CliRunner

from autosd_demo.cli import cli


@patch("autosd_demo.cli.cmd_setup.BuildEnv")
@patch("autosd_demo.cli.cmd_setup.get_executor")
def test_setup_with_local_env(mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = False

    runner = CliRunner()
    runner.invoke(cli, ["setup"])

    mock_build_env.assert_called_once_with(None)
    mock_executor.assert_called_once_with(remote=False, become_required=True)
    mock_executor_ctx.assert_called_once_with(host=None)
    mock_run_playbook.assert_called_once_with(
        "setup.yaml", extra_vars=mock_build_env.return_value
    )


@patch("autosd_demo.cli.cmd_setup.BuildEnv")
@patch("autosd_demo.cli.cmd_setup.get_executor")
def test_setup_with_remote_env(mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = True

    runner = CliRunner()
    runner.invoke(cli, ["setup", "-H", "foobar"])

    mock_executor.assert_called_once_with(remote=True, become_required=True)
    mock_executor_ctx.assert_called_once_with(host="foobar")
    mock_run_playbook.assert_called_once_with(
        "setup.yaml", extra_vars=mock_build_env.return_value
    )
