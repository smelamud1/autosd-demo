from click.testing import CliRunner

from autosd_demo import __version__
from autosd_demo.cli import (
    EXCLUDED_COMMANDS_IN_AUTOSD_PROJECT,
    EXCLUDED_COMMANDS_OUT_AUTOSD_PROJECT,
    MainCLIGroup,
    cli,
)


def test_show_cli_help():
    runner: CliRunner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(cli, ["--help"])
        assert (
            "version" in result.output.strip()
        ), "Version string should should appear in the command list."


def test_version_displays_library_version():
    runner: CliRunner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(cli, ["version"])
        assert (
            __version__ in result.output.strip()
        ), "Version number should match library version."


def test_cli_list_commands_in_autosd_demo(tmp_dir):
    settings_path = tmp_dir / ".autosd-demo.toml"
    settings_path.touch()

    cli_group = MainCLIGroup()
    commands = cli_group.list_commands(None)
    commands_excluded = EXCLUDED_COMMANDS_IN_AUTOSD_PROJECT
    assert set(commands).intersection(commands_excluded) == set()


def test_cli_list_commands_out_autosd_demo(tmp_dir):
    cli_group = MainCLIGroup()
    commands = cli_group.list_commands(None)
    commands_excluded = EXCLUDED_COMMANDS_OUT_AUTOSD_PROJECT
    assert set(commands).intersection(commands_excluded) == set()
