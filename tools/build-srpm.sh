#!/bin/bash
set -e

RPM_PACKAGE_NAME="autosd-demo"
PYTHON_PACKAGE_NAME="autosd_demo"

if [ "$1"x == "--git"x ];
then
  COMMITS_NUM_SINCE_LAST_TAG=$(git log $(git describe --tags --abbrev=0)..HEAD --oneline | wc -l)
  COMMIT_HASH=$(git log -1 --pretty=format:%h)
  export AUTOSD_DEMO_GIT_RELEASE_VERSION=".dev$COMMITS_NUM_SINCE_LAST_TAG+$COMMIT_HASH"
  RELEASE=1$AUTOSD_DEMO_GIT_RELEASE_VERSION
else
  RELEASE=1
fi

flit build --no-use-vcs

DIST_TARBALL=$(find dist -name "$PYTHON_PACKAGE_NAME-*.tar.gz" | head -1)
DIST_TARBALL_FILENAME=$(basename "$DIST_TARBALL")

if [[ $DIST_TARBALL_FILENAME =~ ($PYTHON_PACKAGE_NAME-)([0-9]+\.[0-9]+\.[0-9]+) ]]
then
  VERSION=${BASH_REMATCH[2]}
else
  VERSION="0.0.1"
fi

SPEC_FILENAME="$RPM_PACKAGE_NAME.spec"
SPEC_SRC=$(realpath $(dirname $0)/../rpm/$SPEC_FILENAME)

RPM_DIR=$(rpm --eval %_rpmdir)
SOURCE_DIR=$(rpm --eval %_sourcedir)
SPEC_DIR=$(rpm --eval %_specdir)
SRC_RPM_DIR=$(rpm --eval %_srcrpmdir)
ARCH=$(rpm --eval %_target_cpu)

cp "$DIST_TARBALL" "$SOURCE_DIR/"
cp "$SPEC_SRC" "$SPEC_DIR/"

sed -e "s/define _version 0.0.1/define _version $VERSION/g" -i "$SPEC_DIR/$SPEC_FILENAME"
sed -e "s/define _release 1/define _release $RELEASE/g" -i "$SPEC_DIR/$SPEC_FILENAME"

if [ "$1"x == "--git"x ]
then
    sed -e "s/Source:.*$/Source:         $DIST_TARBALL_FILENAME/g" -i $SPEC_DIR/$SPEC_FILENAME
    sed -e "s/%autosetup -p1/%autosetup -p1 -n $PYTHON_PACKAGE_NAME-$VERSION$AUTOSD_DEMO_GIT_RELEASE_VERSION/g" -i $SPEC_DIR/$SPEC_FILENAME
    cat $SPEC_DIR/$SPEC_FILENAME
fi

rpmbuild -bs $SPEC_DIR/$SPEC_FILENAME
cp "$SRC_RPM_DIR"/$RPM_PACKAGE_NAME-*"$VERSION"*.src.rpm .
