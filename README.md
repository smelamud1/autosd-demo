# AutoSD Demo

## Installation

### Quickstart

You can easily install `autosd-demo` with `dnf`:

```bash
sudo dnf copr enable rmajadas/autosd-demo
sudo dnf install autosd-demo
```

## Usage

Once `autosd-demo` is installed, you can run it in your terminal by using the following command:

```bash
autosd-demo --help
```

## Contribution

If you're interested in contributing to AutoSD Demo, please read the [CONTRIBUTING.md](CONTRIBUTING.md) file in this repository.

## License

AutoSD Demo is released under the MIT License. See the [LICENSE](LICENSE) file for more details.


## Authors

- Roberto Majadas
