# Contribution guidelines

Thank you for considering contributing to the `autosd-demo` project!
TBD

## How to report bugs
TBD

### Security issues
TBD

### Bug reports
TBD

### Features and enhancements
TBD

### Dependencies

#### pre-commit
Install a `pre-commit` framework:
```bash
$ sudo dnf install pre-commit
```
Execute from the `autosd-demo` root folder:
```bash
$ pre-commit install
```
This will install `pre-commit` into your git hooks,
and it will run automatically on every `git commit`.

If you want to manually run all `pre-commit` hooks on a repository, run:
```bash
tox -e pre-commit
```

More info can be found [here](https://pre-commit.com).

### Developer installation

If you're a developer and want to contribute to AutoSD Demo or use the latest development version, follow these steps:

#### Clone the git repository

```bash
git clone https://gitlab.com/roberto-majadas/autosd-demo
```

#### Create a virtual environment:
Optional but recommended:
```bash
python -m venv venv
```

#### Activate the virtual environment:
- On macOS and Linux:
  ```bash
  source venv/bin/activate
  ```

- On Windows:
  ```bash
  .\venv\Scripts\activate
  ```

#### Install `autosd-demo` in development mode:
   ```bash
   pip install -r requirements/dev.txt
   pip install -e .
   ```

### Testing

If you want to enable testing requirements, use the following command:

```bash
pip install -r requirements/dev.txt
```

To run the tests, execute:

```bash
tox -e py
```

### Linters

To run linters code analysis, execute:

```bash
tox -e linters
```

### Code reviews
TBD

### Commit messages
TBD

### Commit access to the `autosd-demo` repository
TBD
